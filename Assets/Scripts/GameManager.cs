using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private GameObject homeBase;


    public GameObject HomeBase
    {
        get => homeBase;
        set
        {
            homeBase = value;
            StartDialoguePhaseThree();
        }
    }

    [Header("PlaneScanning References")]
    [SerializeField] private GameObject planeScanningCanvas;
    [SerializeField] private Toggle groundPlaneToggle;
    [SerializeField] private Button startButton;

    [Header ("Main Game References")]
    [SerializeField] private GameObject mainMenuCanvas;

    [Header("ImageScanning References")]
    [SerializeField] private GameObject imageScanningCanvas;
    [SerializeField] private ARTrackedImageManager arTrackedImageManager;
    [SerializeField] private bool isScanningImage;
    [SerializeField] public GameObject perso;
    public GameObject arCamera;

    [Header("DialogPhaseOne References")]
    [SerializeField] private GameObject phaseOneCanvas;
    [SerializeField] private GameObject done;
    //[SerializeField] private bool isPlanting;
    public bool isPlanting;

    [Header("Day 2 References")]
    [SerializeField] private GameObject day2Canvas;

    [Header("DialogPhaseTwo References")]
    [SerializeField] private GameObject phaseTwoCanvas;

    [Header("Day 7 References")]
    [SerializeField] private GameObject day7Canvas;

    [Header("DialogPhaseThree References")]
    [SerializeField] private GameObject phaseThreeCanvas;

    [Header("EndDemo References")]
    [SerializeField] private GameObject endDemoCanvas;


    private StateMachine stateMachine;
    internal static ARPlane groundPlane;


    //On Awake, on veut checker s'il n'y a pas plusieurs game manager, c'est important qu'il n'y en ait qu'un !
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        stateMachine = new StateMachine();
        /*        stateMachine.ChangeState(new MainMenuState(mainMenuCanvas));*/
        stateMachine.ChangeState(new PlaneScanningState(planeScanningCanvas, groundPlaneToggle, startButton));

    }

    private void Update()
    {
        stateMachine.UpdateState();
    }

    public void StartStory()
    {
        stateMachine.ChangeState(new MainMenuState(mainMenuCanvas));
    }

    public void StartImageScanning()
    {
        stateMachine.ChangeState(new ImageScanningState(imageScanningCanvas, isScanningImage));

    }

    public void StartDialoguePhaseOne()
    {
        stateMachine.ChangeState(new DialogPhaseOne(phaseOneCanvas, done));

    }

    public void StartMenuDay2()
    {
        stateMachine.ChangeState(new MenuDay2State(day2Canvas));

    }

    public void StartDialoguePhaseTwo()
    {
        stateMachine.ChangeState(new DialogPhaseTwo(phaseTwoCanvas));
    }

    public void StartMenuDay7()
    {
        stateMachine.ChangeState(new MenuDay7State(day7Canvas));

    }


    public void StartDialoguePhaseThree()
    {
        stateMachine.ChangeState(new DialogPhaseThree(phaseThreeCanvas));
    }

    public void StartEndDemo()
    {
        stateMachine.ChangeState(new EndDemo(endDemoCanvas));
    }

    public void SetPlanting()
    {
        isPlanting = true;
        Debug.Log("IsPlanting est true !");
    }
}
