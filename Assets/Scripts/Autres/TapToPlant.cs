using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;

public class TapToPlant : MonoBehaviour
{
    public Material clickMat;

    //private Touch touch;
    bool click = false;
    Material originalMat;
    Renderer rend;
    private Transform bourgeon;
    public AudioSource audioPlant;
    public AudioClip plant;


    void Start()
    {
        bourgeon = transform.Find("Bourgeon");
        Debug.Log(bourgeon.name);
        audioPlant = GetComponent<AudioSource>();

        rend = gameObject.GetComponent<Renderer>();
        originalMat = rend.material;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("zonePickable"))
        {
            click = true;
            if (clickMat != null)
            {
                rend.material = clickMat;
            }

        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("zonePickable"))
        {
            click = false;
            if (originalMat != null)
            {
                rend.material = originalMat;
            }
        }
    }


    private void OnMouseDown()
    {
        Debug.Log("Touch�");
        if (click)
        {
            bourgeon.gameObject.SetActive(true);
            rend.material = originalMat;
            audioPlant.PlayOneShot(plant);
            this.gameObject.GetComponent<Collider>().enabled = false;

        }
    }
}
