using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDay2State : IState
{
    private GameObject day2Canvas;


    public MenuDay2State(GameObject day2Canvas)
    {
        this.day2Canvas = day2Canvas;
    }
    public GameObject Day2Canvas
    {
        get => day2Canvas;
        set => day2Canvas = value;
    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter Day2State >>>>>>>>");
        day2Canvas.SetActive(true);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update MainMenuState >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit Day2State >>>>>>>>");
        day2Canvas.SetActive(false);
    }
}
