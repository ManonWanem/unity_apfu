using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{

    public IState currentState;
    public IState previousState;


    public StateMachine()
    {
        Debug.Log("<<<<<<<< StateMachine Created >>>>>>>>");
    }

    public void ChangeState(IState state)
    {
        if(currentState !=null)
        {
            currentState.ExitState();
        }

        previousState = currentState;
        currentState = state;

        state.EnterState();
    }

    public void UpdateState()
    {
        if(currentState != null)
        {
            currentState.UpdateState();
        }
    }

    public void SwitchToPreviousState()
    {
        ChangeState(previousState);
    }
}
