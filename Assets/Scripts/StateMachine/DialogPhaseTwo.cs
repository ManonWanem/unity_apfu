using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogPhaseTwo : IState
{
    private GameObject phaseTwoCanvas;


    public DialogPhaseTwo(GameObject phaseTwoCanvas)
    {
        this.phaseTwoCanvas = phaseTwoCanvas;
    }
    public GameObject PhaseTwoCanvas
    {
        get => phaseTwoCanvas;
        set => phaseTwoCanvas = value;
    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter DialogPhaseTwo State >>>>>>>>");
        phaseTwoCanvas.SetActive(true);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update DialogPhaseTwo State >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit DialogPhaseTwo State >>>>>>>>");
        phaseTwoCanvas.SetActive(false);
    }
}
