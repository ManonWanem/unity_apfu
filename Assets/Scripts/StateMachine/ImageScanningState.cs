using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class ImageScanningState : IState
{
    private GameObject imageScanningCanvas;
    private ARTrackedImageManager arTrackedImageManager;
    //private ARTapToPlace arTapToPlace;

    //private Button beingButton;
    public bool isScanningImage;
    public bool isPersoActive;
    public float posy;
    public Text canvasText;
    public Vector3 pos;
    public AudioSource imgTrgtScanned;


    public ImageScanningState(GameObject imageScanningCanvas, bool isScanningImage)
    {
        this.imageScanningCanvas = imageScanningCanvas;
        this.isScanningImage = isScanningImage;

    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter ImageScanningState >>>>>>>>");
        arTrackedImageManager = GameObject.FindObjectOfType<ARTrackedImageManager>();
        //arTapToPlace = GameObject.FindObjectOfType <ARTapToPlace>();
        imageScanningCanvas.SetActive(true);
        isPersoActive = false;
        isScanningImage = true;
        arTrackedImageManager.trackedImagesChanged += OnImagesChanged;
        imgTrgtScanned = imageScanningCanvas.GetComponent<AudioSource>();



    }

    public void UpdateState()
    {
        if (isPersoActive)
        {
            Debug.Log("<<<<<<<< Update ImageScanningState >>>>>>>>");
            canvasText =  imageScanningCanvas.GetComponentInChildren<Text>();
            canvasText.text = "Look around you and click on the being to begin";
            imgTrgtScanned.enabled = true;
            Being.instance.eventTouched.AddListener(OnBeingTouched);
            isPersoActive = false;

        }


    }

    private void OnImagesChanged(ARTrackedImagesChangedEventArgs data)
    {

        foreach (var image in data.added)
        {

            Debug.Log("<<<<<<<< IMAGE FOUND >>>>>>>>");
            /*arTapToPlace.enabled = true;
            arTapToPlace.OnBeingInstantiated += EnablebeingButton;
            imageScanningCanvas.GetComponentInChildren<TMPro.TMP_Text>().text = "Tap and drag on the Earth image to begin the experience";*/

        }

        foreach (var image in data.updated)
        {

            Debug.Log("position : " + image.transform.position);
            if (image.transform.position.magnitude > 0.1f)
            {
                GameObject perso = GameManager.instance.perso;
                perso.SetActive(true);
                isPersoActive = true;
                //Regler la position du perso
                pos = new Vector3(perso.transform.position.x, image.transform.position.y, perso.transform.position.z);
                perso.transform.position = pos;
                //Regler la rotation du perso

                isScanningImage = false;
                arTrackedImageManager.trackedImagesChanged -= OnImagesChanged;

            }

        }

    }

    /*    private void EnablebeingButton()
        {
            beingButton.gameObject.SetActive(true);
        }*/


    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit ImageScanningState >>>>>>>>");
        imageScanningCanvas.SetActive(false);
        //arTrackedImageManager.trackedImagesChanged -= OnImagesChanged;
        Being.instance.eventTouched.RemoveListener(OnBeingTouched);
        imgTrgtScanned.enabled = false;


    }

    void OnBeingTouched()
    {
        GameManager.instance.StartDialoguePhaseOne();
    }


}
