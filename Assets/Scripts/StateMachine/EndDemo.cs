using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDemo : IState
{
    private GameObject endDemoCanvas;


    public EndDemo(GameObject endDemoCanvas)
    {
        this.endDemoCanvas = endDemoCanvas;
    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter EndDemo >>>>>>>>");
        endDemoCanvas.SetActive(true);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update MainMenuState >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit EndDemo >>>>>>>>");
        endDemoCanvas.SetActive(false);
    }
}
