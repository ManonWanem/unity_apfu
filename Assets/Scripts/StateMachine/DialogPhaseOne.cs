using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogPhaseOne : IState
{
    private GameObject phaseOneCanvas;
    private GameObject done;
    //private bool isActive = false;
    GameObject perso = GameManager.instance.perso;

    public DialogPhaseOne(GameObject phaseOneCanvas, GameObject done)
    {
        this.phaseOneCanvas = phaseOneCanvas;
        this.done = done;
    }
    //public GameObject DialogPhaseOneCanvas
    //{
    //    get => phaseOneCanvas;
    //    set => phaseOneCanvas = value;
    //}


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter DialogPhaseOne State >>>>>>>>");
        phaseOneCanvas.SetActive(true);
        Being.instance.eventTouched.AddListener(OnBeingTouched);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update DialogPhaseOne State >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit DialogPhaseOne State >>>>>>>>");
        phaseOneCanvas.SetActive(false);
        Being.instance.eventTouched.RemoveListener(OnBeingTouched);
        done.SetActive(false);

    }

    void OnBeingTouched()
    {
        if (GameManager.instance.isPlanting)
        {
            done.SetActive(true);
            Debug.Log("Done ?");
        }
    }


}

