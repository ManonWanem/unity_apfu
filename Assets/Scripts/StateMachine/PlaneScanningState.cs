using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlaneScanningState : IState
{

    private Toggle groundPlaneToggle;
    private Button startButton;
    private GameObject planeScanningCanvas;
    //private GameObject enemyBase;
    private GameObject character;


    private ARPlaneManager arPlaneManager;
    //private List<ARPlane> foundPlanes;
    private ARPlane groundPlane;

    //private GameObject enemyBasePrefab;
    private GameObject perso;

    public PlaneScanningState(GameObject planeScanningCanvas, Toggle groundPlaneToggle, Button startButton)
    {
        this.planeScanningCanvas = planeScanningCanvas;
        this.groundPlaneToggle = groundPlaneToggle;
        this.startButton = startButton;

    }
    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter PlaneScanning State >>>>>>>>");
        planeScanningCanvas.SetActive(true);
        arPlaneManager = GameObject.FindObjectOfType<ARPlaneManager>();
        arPlaneManager.enabled = true;
        arPlaneManager.planesChanged += PlanesChanged;
        //foundPlanes = new List<ARPlane>();
        groundPlaneToggle.isOn = false;
        startButton.interactable = false;


    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update PlaneScanning State >>>>>>>>");



    }

    private void PlanesChanged(ARPlanesChangedEventArgs data)
    {
        //do lots of funny stuff with the planes...
        if (data.added != null && data.added.Count > 0)
        {
            //foundPlanes.AddRange(data.added);
            foreach (ARPlane plane in data.added)
            {
                Debug.Log("Plane surface : " + (plane.extents.x * plane.extents.y));
                //plane qui serait le ground
                bool bigEnough = plane.extents.x * plane.extents.y >= 0.1f;

                if (/*bigEnough && */(groundPlane == null || plane.transform.position.y < groundPlane.transform.position.y))
                {
                    //Si on avait d�j� trouv� un ground plus haut, on le d�sactive
                    if (groundPlane != null)
                    {
                        groundPlane.gameObject.SetActive(false);
                    }

                    //On a trouv� un ground ou une autre surface plus basse, qui a �t� mise � jour
                    groundPlane = plane;
                    groundPlaneToggle.isOn = true;
                    // Pour activer directement le button de start
                    startButton.interactable = true;
                }
                else
                {
                    //plane.gameObject.SetActive(false);
                }
            }
        }
    }


    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit PlaneScanning State >>>>>>>>");
        planeScanningCanvas.SetActive(false);
        arPlaneManager.planesChanged -= PlanesChanged;
        arPlaneManager.enabled = false;
        GameManager.groundPlane = groundPlane;
    }
}
