using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuState : IState
{
    private GameObject mainMenuCanvas;


    public MainMenuState(GameObject mainMenuCanvas)
    {
        this.mainMenuCanvas = mainMenuCanvas;
    }
    public GameObject MainMenuCanvas { 
        get => mainMenuCanvas;
        set => mainMenuCanvas = value;
    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter MainMenuState >>>>>>>>");
        mainMenuCanvas.SetActive(true);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update MainMenuState >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit MainMenuState >>>>>>>>");
        mainMenuCanvas.SetActive(false);
    }
}
