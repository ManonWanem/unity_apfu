using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogPhaseThree : IState
{
    private GameObject phaseThreeCanvas;


    public DialogPhaseThree(GameObject phaseThreeCanvas)
    {
        this.phaseThreeCanvas = phaseThreeCanvas;
    }
    public GameObject PhaseThreeCanvas
    {
        get => phaseThreeCanvas;
        set => phaseThreeCanvas = value;
    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter DialogPhaseThree State >>>>>>>>");
        phaseThreeCanvas.SetActive(true);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update DialogPhaseThree State >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit DialogPhaseThree State >>>>>>>>");
        phaseThreeCanvas.SetActive(false);
    }
}

