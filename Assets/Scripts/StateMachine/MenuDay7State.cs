using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDay7State : IState

{
    private GameObject day7Canvas;


    public MenuDay7State(GameObject day7Canvas)
    {
        this.day7Canvas = day7Canvas;
    }
    public GameObject Day7Canvas
    {
        get => day7Canvas;
        set => day7Canvas = value;
    }


    public void EnterState()
    {
        Debug.Log("<<<<<<<< Enter Day7State >>>>>>>>");
        day7Canvas.SetActive(true);
    }

    public void UpdateState()
    {
        //Debug.Log("<<<<<<<< Update MainMenuState >>>>>>>>");
    }

    public void ExitState()
    {
        Debug.Log("<<<<<<<< Exit Day7State >>>>>>>>");
        day7Canvas.SetActive(false);
    }
}
